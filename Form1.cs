﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cassandra_DB_Tool
{
    public partial class Form1 : Form
    {
        private volatile bool stopToken;
        private volatile string selectedSourceKeySpace ;
        private volatile string selectedTargetKeyspace ;
        private Thread thread;
        private delegate void AddMessageDelegate(string message);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                AddMessage("Copying cqlsh.py to Cassandra bin folder...");
                File.Copy(@"cqlsh/cqlsh.py", @"C:\Program Files\apache-cassandra-3.11.2\bin\cqlsh.py", true);
                AddMessage("Connecting to cassandra database...");
                AddMessage("Listing keyspaces...");
                sourceKeyspacesList.DataSource = GetKeySpacesList();
                sourceTablesList.DataSource = GetKeySpaceTablesList(sourceKeyspacesList.SelectedItem.ToString());
                targetKeyspacesList.DataSource = GetKeySpacesList();
                targetTablesList.DataSource = GetKeySpaceTablesList(targetKeyspacesList.SelectedItem.ToString());
            }
            catch (Exception exception)
            {
                AddMessage("ERROR: " + exception.Message);
            }
            
        }

        private void sourceKeyspacesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedKeyspace = sourceKeyspacesList.SelectedItem.ToString();
                sourceTablesList.DataSource = null;
                sourceTablesList.Items.Clear();
                sourceTablesList.DataSource = GetKeySpaceTablesList(selectedKeyspace);
                AddMessage("Source tables loaded : " + selectedKeyspace);
            }
            catch (Exception exception)
            {
                AddMessage("ERROR: " + exception.Message);
            }
            
        }
        private void targetKeyspacesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedKeyspace = targetKeyspacesList.SelectedItem.ToString();
                targetTablesList.DataSource = null;
                targetTablesList.SelectionMode = SelectionMode.One;
                targetTablesList.Items.Clear();
                targetTablesList.DataSource = GetKeySpaceTablesList(selectedKeyspace);
                targetTablesList.SelectionMode = SelectionMode.None;
                AddMessage("Target tables loaded : " + selectedKeyspace);
            }
            catch (Exception exception)
            {
                AddMessage("ERROR: " + exception.Message);
            }
           
        }
        private void startMigrationBtn_Click(object sender, EventArgs e)
        {
            selectedSourceKeySpace = sourceKeyspacesList.SelectedItem.ToString();
            selectedTargetKeyspace = targetKeyspacesList.SelectedItem.ToString();
            startMigrationBtn.Enabled = false;
            thread = new Thread(Start);
            thread.Start();

        }

        private void Start()
        {
            try
            {
                AddMessage("Starting data migration from " + selectedSourceKeySpace + " to " + selectedTargetKeyspace);

                foreach (var item in sourceTablesList.SelectedItems)
                {
                    string table = item.ToString();
                    AddMessage("Exporting " + table + " from " + selectedSourceKeySpace);
                    CopyFromTable(selectedSourceKeySpace, table, selectedSourceKeySpace + "." + table + ".csv");
                    AddMessage("Importing " + table + " to " + selectedTargetKeyspace);
                    CopyToTable(selectedTargetKeyspace, table, selectedSourceKeySpace + "." + table + ".csv");
                    Thread.Sleep(500);
                    if (stopToken)
                    {
                        stopToken = false;
                        break;
                    }
                }
                AddMessage("Data migration completed successfully!");
            }
            catch (Exception exception)
            {
                AddMessage("ERROR: " + exception.Message);
            }
        }

        private void stopMigrationBtn_Click(object sender, EventArgs e)
        {
            stopToken = true;
            AddMessage("Stopping migration...");
            thread.Abort();
            thread = null;
            stopToken = false;
            startMigrationBtn.Enabled = true;
        }

        private void AddMessage(string message)
        {
            if (messagesTextBox.InvokeRequired)
            {
                var d = new AddMessageDelegate(AddMessage);
                Invoke(d, message);
            }
            else
            {
                messagesTextBox.Text += message + Environment.NewLine;
            }
            
        }
        private void messagesTextBox_TextChanged(object sender, EventArgs e)
        {
            messagesTextBox.SelectionStart = messagesTextBox.Text.Length;
            messagesTextBox.ScrollToCaret();
        }

        static string RunCqlshCommand(string cmd)
        {
            StringBuilder output = new StringBuilder();
            DirectoryInfo di = Directory.CreateDirectory(@"C:/Cassandra tool");
            var process = new Process();
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:/Python27/python.exe";
            psi.Arguments = @"""C:/Program Files/apache-cassandra-3.11.2/bin/cqlsh.py""";
            psi.RedirectStandardInput = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.WorkingDirectory = @"C:/Cassandra tool";
            process.StartInfo = psi;
            process.Start();
            process.OutputDataReceived += (sender, e) => {
                Console.WriteLine(e.Data);
                output.Append(e.Data);
            };
            //process.ErrorDataReceived += (sender, e) => { Console.WriteLine(e.Data); };

            process.BeginOutputReadLine();
            //process.BeginErrorReadLine();
            using (StreamWriter sw = process.StandardInput)
            {
                sw.WriteLine(cmd);
            }

            process.WaitForExit();
            return output.ToString();
        }

        public static List<string> GetKeySpacesList()
        {
            string result = RunCqlshCommand("DESCRIBE keyspaces");
            List<string> keySpacesList = result.Split(' ').ToList();
            keySpacesList.RemoveAll(s => string.IsNullOrWhiteSpace(s));
            keySpacesList.RemoveAll(x => x.StartsWith(@"system"));
            return keySpacesList;
        }

        public static List<string> GetKeySpaceTablesList(string keySpace)
        {
            string result = RunCqlshCommand(string.Format("use {0}; DESC TABLES", keySpace));
            List<string> keySpaceTablesList = result.Split(' ').ToList();
            keySpaceTablesList.RemoveAll(s => string.IsNullOrWhiteSpace(s));
            return keySpaceTablesList;
        }

        public static bool CopyFromTable(string keySpace, string table, string csvFilePath)
        {
            string result = RunCqlshCommand(string.Format("COPY {0}.{1} TO '{2}' WITH HEADER=TRUE;", keySpace, table, csvFilePath));

            return true;
        }

        public static bool CopyToTable(string keySpace, string table, string csvFilePath)
        {
            string result = RunCqlshCommand(string.Format("COPY {0}.{1} FROM '{2}' WITH CHUNKSIZE=1;", keySpace, table, csvFilePath));

            return true;
        }
    }
}
