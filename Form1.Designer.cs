﻿namespace Cassandra_DB_Tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sourceTablesList = new System.Windows.Forms.ListBox();
            this.sourceKeyspacesList = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.messagesTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.targetTablesList = new System.Windows.Forms.ListBox();
            this.targetKeyspacesList = new System.Windows.Forms.ListBox();
            this.startMigrationBtn = new System.Windows.Forms.Button();
            this.stopMigrationBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.sourceTablesList);
            this.groupBox1.Controls.Add(this.sourceKeyspacesList);
            this.groupBox1.Location = new System.Drawing.Point(25, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 255);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Source";
            // 
            // sourceTablesList
            // 
            this.sourceTablesList.FormattingEnabled = true;
            this.sourceTablesList.Location = new System.Drawing.Point(22, 148);
            this.sourceTablesList.Name = "sourceTablesList";
            this.sourceTablesList.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.sourceTablesList.Size = new System.Drawing.Size(219, 95);
            this.sourceTablesList.TabIndex = 1;
            // 
            // sourceKeyspacesList
            // 
            this.sourceKeyspacesList.FormattingEnabled = true;
            this.sourceKeyspacesList.Location = new System.Drawing.Point(22, 32);
            this.sourceKeyspacesList.Name = "sourceKeyspacesList";
            this.sourceKeyspacesList.Size = new System.Drawing.Size(219, 95);
            this.sourceKeyspacesList.TabIndex = 0;
            this.sourceKeyspacesList.SelectedIndexChanged += new System.EventHandler(this.sourceKeyspacesList_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.messagesTextBox);
            this.groupBox2.Location = new System.Drawing.Point(25, 331);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(594, 141);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Messages";
            // 
            // messagesTextBox
            // 
            this.messagesTextBox.Location = new System.Drawing.Point(28, 17);
            this.messagesTextBox.Multiline = true;
            this.messagesTextBox.Name = "messagesTextBox";
            this.messagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messagesTextBox.Size = new System.Drawing.Size(539, 116);
            this.messagesTextBox.TabIndex = 0;
            this.messagesTextBox.TextChanged += new System.EventHandler(this.messagesTextBox_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.targetTablesList);
            this.groupBox3.Controls.Add(this.targetKeyspacesList);
            this.groupBox3.Location = new System.Drawing.Point(334, 67);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(284, 255);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target";
            // 
            // targetTablesList
            // 
            this.targetTablesList.FormattingEnabled = true;
            this.targetTablesList.Location = new System.Drawing.Point(29, 148);
            this.targetTablesList.Name = "targetTablesList";
            this.targetTablesList.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.targetTablesList.Size = new System.Drawing.Size(219, 95);
            this.targetTablesList.TabIndex = 3;
            // 
            // targetKeyspacesList
            // 
            this.targetKeyspacesList.FormattingEnabled = true;
            this.targetKeyspacesList.Location = new System.Drawing.Point(29, 32);
            this.targetKeyspacesList.Name = "targetKeyspacesList";
            this.targetKeyspacesList.Size = new System.Drawing.Size(219, 95);
            this.targetKeyspacesList.TabIndex = 2;
            this.targetKeyspacesList.SelectedIndexChanged += new System.EventHandler(this.targetKeyspacesList_SelectedIndexChanged);
            // 
            // startMigrationBtn
            // 
            this.startMigrationBtn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.startMigrationBtn.Location = new System.Drawing.Point(421, 478);
            this.startMigrationBtn.Name = "startMigrationBtn";
            this.startMigrationBtn.Size = new System.Drawing.Size(97, 34);
            this.startMigrationBtn.TabIndex = 2;
            this.startMigrationBtn.Text = "Start Migration";
            this.startMigrationBtn.UseVisualStyleBackColor = false;
            this.startMigrationBtn.Click += new System.EventHandler(this.startMigrationBtn_Click);
            // 
            // stopMigrationBtn
            // 
            this.stopMigrationBtn.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.stopMigrationBtn.Location = new System.Drawing.Point(524, 478);
            this.stopMigrationBtn.Name = "stopMigrationBtn";
            this.stopMigrationBtn.Size = new System.Drawing.Size(95, 34);
            this.stopMigrationBtn.TabIndex = 3;
            this.stopMigrationBtn.Text = "Stop Migration";
            this.stopMigrationBtn.UseVisualStyleBackColor = false;
            this.stopMigrationBtn.Click += new System.EventHandler(this.stopMigrationBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(593, 53);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(301, 166);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 59);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Location = new System.Drawing.Point(108, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tables";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(115, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tables";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(96, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Keyspaces";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(104, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Keyspaces";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(642, 524);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.stopMigrationBtn);
            this.Controls.Add(this.startMigrationBtn);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cassandra Migration Tool";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox sourceKeyspacesList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox sourceTablesList;
        private System.Windows.Forms.ListBox targetTablesList;
        private System.Windows.Forms.ListBox targetKeyspacesList;
        private System.Windows.Forms.TextBox messagesTextBox;
        private System.Windows.Forms.Button startMigrationBtn;
        private System.Windows.Forms.Button stopMigrationBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
    }
}

